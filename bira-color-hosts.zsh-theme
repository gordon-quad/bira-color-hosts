# ZSH Theme - Preview: http://gyazo.com/8becc8a7ed5ab54a0262a470555c3eed.png
local return_code="%(?..%{$fg[red]%}%? ↵%{$reset_color%})"

colmap=(
    green
    yellow
    white
    magenta
    cyan
)

hostmarkcolmap=(
    green
    yellow
    white
    magenta
    cyan
    blue
    black
    red
)
hostmarkmap=(
    ▌
    ▄
    █
)

function _hash() {
    local chash=5381
    foreach letter ( ${(ws::)1[(ws:.:)1]} )
      (( chash = (((chash << 5) + chash ) + #letter) & 0xffff ))
    end
    echo $chash
}
    
function _username_color() {
    local chash=$(_hash ${USER}$(_hash $HOST))
    local crand=$(( $chash % $#colmap + 1 ))
    local crandname=$colmap[$crand]
    echo "${fg[$crandname]}"
}

function _hostname_color() {
    local chash=$(_hash $HOST)
    local crand=$(( $chash % $#colmap + 1 ))
    local crandname=$colmap[$crand]
    echo "${fg[$crandname]}"
}

function _hostmark() {
    local chash=$(_hash $HOST)
    local chash1=$(_hash $chash)
    local chash2=$(_hash $chash1)
    local chash3=$(_hash $chash2)
    local chash4=$(_hash $chash3)
    local chash5=$(_hash $chash4)
    local chash6=$(_hash $chash5)
    local chash7=$(_hash $chash6)
    local chash8=$(_hash $chash7)
    local chash9=$(_hash $chash8)
    local crand1=$(( $chash1 % $#hostmarkcolmap + 1 ))
    local crand2=$(( $chash2 % $#hostmarkcolmap + 1 ))
    local crand3=$(( $chash3 % $#hostmarkcolmap + 1 ))
    local crand4=$(( $chash4 % $#hostmarkcolmap + 1 ))
    local crand5=$(( $chash5 % $#hostmarkcolmap + 1 ))
    local crand6=$(( $chash6 % $#hostmarkcolmap + 1 ))
    local crand7=$(( $chash7 % $#hostmarkmap + 1 ))
    local crand8=$(( $chash8 % $#hostmarkmap + 1 ))
    local crand9=$(( $chash9 % $#hostmarkmap + 1 ))
    local bgcol1=$bg[$hostmarkcolmap[$crand1]]
    local bgcol2=$bg[$hostmarkcolmap[$crand2]]
    local bgcol3=$bg[$hostmarkcolmap[$crand3]]
    local fgcol1=$fg[$hostmarkcolmap[$crand4]]
    local fgcol2=$fg[$hostmarkcolmap[$crand5]]
    local fgcol3=$fg[$hostmarkcolmap[$crand6]]
    local char1=$hostmarkmap[$crand7]
    local char2=$hostmarkmap[$crand8]
    local char3=$hostmarkmap[$crand9]
    echo "%{$fgcol1$bgcol1%}$char1%{$fgcol2$bgcol2%}$char2%{$fgcol3$bgcol3%}$char3"
}

user_color=$(_username_color)
host_color=$(_hostname_color)
host_mark=$(_hostmark)


if [[ $UID -eq 0 ]]; then
    local user_host='%{$terminfo[bold]$fg[red]%}%n%{$reset_color%}@%{$terminfo[bold]$host_color%}%m%{$reset_color%} $host_mark%{$reset_color%}'
else
    local user_host='%{$terminfo[bold]$user_color%}%n%{$reset_color%}@%{$terminfo[bold]$host_color%}%m%{$reset_color%} $host_mark%{$reset_color%}'
fi

local current_dir='%{$terminfo[bold]$fg[blue]%} %~%{$reset_color%}'
local git_branch='$(git_prompt_info)%{$reset_color%}'
local vim='${MODE_INDICATOR_PROMPT}'

PROMPT="╭─${user_host} ${current_dir} ${git_branch} ${vim}
╰─%B$%b "
RPS1="${return_code}"

ZSH_THEME_GIT_PROMPT_PREFIX="%{$fg[yellow]%}‹"
ZSH_THEME_GIT_PROMPT_SUFFIX="› %{$reset_color%}"
